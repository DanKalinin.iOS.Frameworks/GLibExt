//
//  NseCancellable.h
//  GLibExt
//
//  Created by Dan on 10.07.2021.
//

#import "NseObject.h"

@interface NseCancellable : NseObject

@property GCancellable *object;

+ (instancetype)cancellable;

- (void)cancel;

@end
