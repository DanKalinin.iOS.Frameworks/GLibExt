//
//  NseDictionary.m
//  GLibExt
//
//  Created by Dan on 11.12.2022.
//

#import "NseDictionary.h"

@implementation NSDictionary (GHashTable)

- (GHashTable *)gHashTableGchararrayGchararrayTo {
    GHashTable *ret = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
    
    for (NSString *key in self) {
        NSString *value = self[key];
        gchar *sdkKey = g_strdup(key.UTF8String);
        gchar *sdkValue = g_strdup(value.UTF8String);
        (void)g_hash_table_insert(ret, sdkKey, sdkValue);
    }
    
    return ret;
}

@end
