//
//  NseDate.m
//  GLibExt
//
//  Created by Dan on 03.07.2021.
//

#import "NseDate.h"

@implementation NSDate (GDateTime)

+ (instancetype)gDateTimeFrom:(GDateTime *)sdkDateTime {
    gint64 seconds = g_date_time_to_unix(sdkDateTime);
    NSDate *ret = [NSDate dateWithTimeIntervalSince1970:seconds];
    return ret;
}

@end
